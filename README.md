# `pseudo_tilt` Rust Crate

This library is for computing objects which can potentially give rise to walls in Bridgeland-stability for coherent sheaves on Picard rank 1 surfaces.

This is currently only attempted in cases where it is known that there are only finitely many, but will potentially be extended to provide generating sequences in the case of principally polarized abelian surfaces.

## Docs

Latest `cargo docs` built on main branch:
[link](https://pseudowalls.gitlab.io/tilt.rs/pseudo_tilt)

## Usage

### Binary executable:

#### Build
Requires rust nightly to build (if using [rustup](https://rustup.rs/), available via `rustup install nightly`).

... to come
#### Download
Executables are built as per steps in [.gitlab-ci.yml](.gitlab-ci.yml) for the following targets:
- [Windows x86_64](https://gitlab.com/api/v4/projects/45109089/jobs/artifacts/main/raw/target/x86_64-pc-windows-gnu/release/pseudo_tilt.exe?job=x64-executables)
- [Linux x86_64](https://gitlab.com/api/v4/projects/45109089/jobs/artifacts/main/raw/target/x86_64-unknown-linux-musl/release/pseudo_tilt?job=x64-executables)
- [Linux arm64](https://gitlab.com/api/v4/projects/45109089/jobs/artifacts/main/raw/target/aarch64-unknown-linux-musl/release/pseudo_tilt?job=arm-executable)

Example CLI usage:

Windows
```batch
pseudo_tilt.exe --semistabilizers 3 2 -4
```
Linux
```sh
chmod +x pseudo_tilt_walls_rs
./pseudo_tilt --semistabilizers 3 2 -4
```
Start of expected output:
```stdout
v = Chern Character: (3, 2ℓ, -4½ℓ²)
Δ(v) = 16
β_(v) = -2/3

pseudo-semistabilizers:
Chern Character: (1, 0ℓ, 0½ℓ²)
Chern Character: (4, -2ℓ, 1½ℓ²)
...

```
Call executable without arguments for usage instructions


### As Python module:

There's a Python wrapper [here](https://github.com/lnay/pseudo_tilt_py), which is pip-installable: `pip install pseudo_bridgeland_tilt`.
See example usage on other repo.

### Webapp:

There's a wasm webapp wrapping around this library [here](https://gitlab.com/pseudowalls/webapp/tilt.sycamore/) and hosted on gitlab pages [here](https://pseudowalls.gitlab.io/webapp/tilt.sycamore/) (currently crashes often due to panics in the [issues]).

