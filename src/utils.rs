use num::{rational::Rational64, integer::Roots};
use std::cmp::Ordering::{Equal, Greater, Less};

/// Returns least integer strictly greater than `frac`
pub fn least_greater_int(frac: Rational64) -> i64 {
    if frac.is_integer() {
        return frac.to_integer() + 1;
    }
    // Otherwise, not an int

    // Sign of `frac` needs to be checked because `to_integer` rounds towards
    // 0 instead of 'up' or 'down' consistently
    match frac.cmp(&Rational64::from_integer(0)) {
        Greater => frac.to_integer() + 1,
        Less => frac.to_integer(),
        Equal => 1, // should already be covered by integer case
    }
}

/// Returns greatest integer less than, or equal to, `frac`
pub fn greatest_lesser_or_eq_int(frac: Rational64) -> i64 {
    if frac.is_integer() {
        return frac.to_integer();
    }
    // Otherwise, not an int

    // Sign of `frac` needs to be checked because `to_integer` rounds towards
    // 0 instead of 'up' or 'down' consistently
    match frac.cmp(&Rational64::from_integer(0)) {
        Greater => frac.to_integer(),
        Less => frac.to_integer() - 1,
        Equal => -1, // should already be covered by integer case
    }
}

/// Crude placeholder implementation of modular inverse
/// Returns m s.t. (n*m) % modulus == 1 if exists, else None
pub fn modulo_inverse(n: i64, modulus: i64) -> Result<i64, &'static str> {
    if n < 0 || modulus <= 1 {
        // THOUGHT: maybe better to have upfront typechecking via `modulus`
        // crate types
        return Err("both arguments to modulo_inverse must be positive");
    }
    let mut inverse = 1;
    loop {
        let next = (inverse * n) % modulus;
        if next == 1 {
            return Ok(inverse);
        }
        if next == 0 {
            return Err("modulo inverse does not exist");
        }
        inverse = next;
    }
}

// Custom alternative to rarges like (1..), which allow for infinite
// sequences, but with negative step

struct LinearSeq<VAL, STEP> where VAL: std::ops::AddAssign<STEP> + Copy {
    current: VAL,
    step: STEP,
}

impl<VAL, STEP: Copy> Iterator for LinearSeq<VAL, STEP>
where VAL: std::ops::AddAssign<STEP> + Copy {
    type Item = VAL;

    fn next(&mut self) -> Option<Self::Item> {
        let result = self.current;
        self.current += self.step;
        Some(result)
    }
}

pub fn linear_seq<VAL, STEP: Copy>(start: VAL, step: STEP)
-> impl Iterator<Item = VAL>
where VAL: std::ops::AddAssign<STEP> + Copy {
    LinearSeq {
        current: start,
        step,
    }
}

// FUTURE: implement something like this for Chern2 directly
// (in chern_character::terms)
pub fn checked_sqrt(q: Rational64) -> Option<Rational64> {
    let numer = q.numer();
    let denom = q.denom();
    match (checked_sqrt_i64(*numer), checked_sqrt_i64(*denom)) {
        (Some(a), Some(b)) => Some((a, b).into()),
        _ => None,
    }
}

fn checked_sqrt_i64(n: i64) -> Option<i64> {
    if n < 0 {
        return None;
    }
    let potential_sqrt = n.sqrt();
    if potential_sqrt * potential_sqrt == n {
        Some(potential_sqrt)
    } else {
        None
    }
}
