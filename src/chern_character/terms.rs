use std::fmt::{Display, Formatter};
use num::rational::Rational64;
use serde::{Serialize, Deserialize};
use crate::utils::{greatest_lesser_or_eq_int, least_greater_int};

pub type Chern0 = i64;

// Chern 1 and type conversions //
// ===== = === ==== =========== //
#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Chern1 {
    coeff: i64, // multiple of ℓ
}

impl From<i64> for Chern1 {
    fn from(coeff: i64) -> Self {
        Self{coeff}
    }
}

impl From<Chern1> for i64 {
    fn from(ch1: Chern1) -> i64 {
        ch1.coeff
    }
}

// Chern 2 and type conversions //
// ===== = === ==== =========== //

// M must be a positive even integer
#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Chern2<const M: u32> {
    coeff: i64, // multiple of ℓ²/M
}

impl<const M: u32> From<i64> for Chern2<M> {
    fn from(coeff: i64) -> Self {
        Self{coeff}
    }
}

impl<const M: u32> Chern2<M> {
    pub fn ℓ2_coeff(self) -> Rational64 {
        (self.coeff, M as i64).into()
    }
    pub fn raw_int_repn(self) -> i64 {
        self.coeff
    }
}

pub fn least_greater_chern2<const M: u32>(rat: Rational64) -> Chern2<M> {
    least_greater_int(rat * (M as i64)).into()
}

pub fn greatest_lesser_or_eq_chern2<const M: u32>(rat: Rational64) -> Chern2<M> {
    greatest_lesser_or_eq_int(rat * (M as i64)).into()
}

pub fn inclusive_range<const M: u32>(start: Chern2<M>, end: Chern2<M>)
-> impl Iterator<Item=Chern2<M>> {
    (start.coeff..=end.coeff)
        .map(|c| c.into())
}

// Multiplications between Chern Terms //
// =============== ======= ===== ===== //

// define multiplication between Chern0 and Chern1
impl std::ops::Mul<Chern1> for Chern0 {
    type Output = Chern1;
    fn mul(self, other: Chern1) -> Self::Output {
        Self::Output{ coeff: self * other.coeff }
    }
}
// define multiplication between Chern0 and Chern2
impl<const M: u32> std::ops::Mul<Chern2<M>> for Chern0 {
    type Output = Chern2<M>;
    fn mul(self, other: Chern2<M>) -> Self::Output {
        Self::Output{ coeff: self * other.coeff }
    }
}

// This couldn't be done with an operator because generic parameter of output
// not determined by inputs
pub fn cup_product<const M: u32>(a: Chern1, b: Chern1) -> Chern2<M> {
    Chern2::<M>{ coeff: a.coeff * b.coeff * (M as i64) }
}

// Addition/Subtraction among Chern Terms //
// ==================== ===== ===== ===== //

// define addition of Chern terms
impl std::ops::Add for Chern1 {
    type Output = Chern1;
    fn add(self, rhs: Chern1) -> Self::Output {
        Self { coeff: self.coeff + rhs.coeff }
    }
}
impl std::ops::Sub for Chern1 {
    type Output = Chern1;
    fn sub(self, rhs: Chern1) -> Self::Output {
        Self { coeff: self.coeff - rhs.coeff }
    }
}
impl std::ops::AddAssign for Chern1 {
    fn add_assign(&mut self, rhs: Chern1) {
        self.coeff += rhs.coeff;
    }
}

impl<const M: u32> std::ops::Add for Chern2<M> {
    type Output = Chern2<M>;
    fn add(self, rhs: Self) -> Self::Output {
        Self { coeff: self.coeff + rhs.coeff }
    }
}
impl<const M: u32> std::ops::Sub for Chern2<M> {
    type Output = Chern2<M>;
    fn sub(self, rhs: Self) -> Self::Output {
        Self { coeff: self.coeff - rhs.coeff }
    }
}

// Display for Chern Terms //
// ======= === ===== ===== //

impl Display for Chern1 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}ℓ", self.coeff)
    }
}
impl<const M: u32> Display for Chern2<M> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}ℓ²", self.ℓ2_coeff())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn chern_terms() {
        let r: Chern0 = 1.into();
        let c: Chern1 = 2.into();
        let d: Chern2::<2> = (2*2*2).into();
        assert_eq!(r * r, r);
        assert_eq!(r * c, c);
        assert_eq!(cup_product::<2>(c, c), d);
        assert_eq!(Rational64::from_integer(2*2), d.ℓ2_coeff());
    }

    #[test]
    fn display() {
        let r: Chern0 = 1.into();
        let c: Chern1 = 2.into();
        let d: Chern2<2> = (2*2*2).into();
        assert_eq!(format!("{}", r), "1");
        assert_eq!(format!("{}", c), "2ℓ");
        assert_eq!(format!("{}", d), "4ℓ²");
    }

    #[test]
    fn chern2_ranges() {
        let start = least_greater_chern2::<2>((-1,3).into());
        let end = greatest_lesser_or_eq_chern2::<2>((1,3).into());
        let range = inclusive_range(start, end).collect::<Vec<_>>();
        assert_eq!(
            range,
            vec![
                0.into(),
            ]
        );
        let start = least_greater_chern2::<4>((-1,3).into());
        let end = greatest_lesser_or_eq_chern2::<4>((1,3).into());
        let range = inclusive_range(start, end).collect::<Vec<_>>();
        assert_eq!(
            range,
            vec![
                (-1).into(),
                0.into(),
                1.into(),
            ]
        );
    }
}
