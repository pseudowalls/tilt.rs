use super::chern_character::{ChernChar, Δ};
use num::CheckedDiv;
use num::rational::Rational64;
use crate::utils::checked_sqrt;

pub mod left_pseudo_semistabilizers;
pub mod wall;
pub mod twisted;

extern crate test;

/// Returns β_ for given Chern character v, that is:
/// ```equation
///            _________
///       c - √ c² - 2rd
/// β_  = --------------   for v = (r, c⋅ℓ, d⋅ℓ²) and r≠0
///             r
///
///        d
/// β_  = ---              for v = (0, c⋅ℓ, d⋅ℓ²) and c≠0
///        c
///
/// β_  = +∞               otherwise
///```
/// ..., but only if rational (ϵℚ), otherwise returns None
pub fn β_<const V: u32>(v: &ChernChar<V>) -> Option<Rational64> {
    if v.r == 0 {
        // Rank 0 case uses alternative formula, and returns None
        // when divides by 0
        return v.d.ℓ2_coeff()
            .checked_div(&Rational64::from_integer(v.c.into()));
        // ^ β_ = +∞ considered here with potential `None` return
    }
    // term inside of sqrt in β_ formula:
    let sqrt_part: Rational64 = checked_sqrt(Δ(&v).ℓ2_coeff())?;

    Some(
        (Rational64::from_integer(v.c.into()) - sqrt_part) / v.r
    )
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn beta_minus() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (3, 2, -4).into();
        assert_eq!(Δ(&v), (16 * M as i64).into());
        assert_eq!(β_(&v), Some(Rational64::from(-2) / 3));
        // rank 0 case
        let v: ChernChar::<M>  = (0, 2, -4).into();
        assert_eq!(Δ(&v), (4 * M as i64).into());
        assert_eq!(β_(&v), Some(Rational64::from(-1)));
    }
    // TODO test considered_q
    #[test]
    fn wall() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (1, 0, 0).into();
        let u: ChernChar::<M> = (1, 1, 1).into();
        assert_eq!(wall::centre(&v, &u), Some(Rational64::from_integer(1) / 2));
        assert_eq!(wall::radius2(&v, &u), Some(Rational64::from_integer(1) / 4));
        // rank 0 case
        let v: ChernChar::<M> = (0, 1, 0).into();
        let u: ChernChar::<M> = (1, 1, 1).into();
        assert_eq!(wall::centre(&v, &u), Some(Rational64::from_integer(0)));
        assert_eq!(wall::radius2(&v, &u), Some(Rational64::from_integer(1)));
        let v: ChernChar::<M> = (0, 1, 0).into();
        let u: ChernChar::<M> = (4, 2, 1).into();
        assert_eq!(wall::centre(&v, &u), Some(Rational64::from_integer(0)));
        assert_eq!(wall::radius2(&v, &u), Some(Rational64::from_integer(1)/4));
    }

    #[test]
    fn structure_sheaf_example() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (1, 0, 0).into();
        let semistabilizers = left_pseudo_semistabilizers::find_all(&v);
        assert_eq!(semistabilizers, Ok(vec![]));
    }

    #[test]
    fn nontrivial_example() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (3, 2, -4).into();
        let semistabilizers = left_pseudo_semistabilizers::find_all(&v);
        assert_eq!(
            semistabilizers,
            Ok(vec![
                (1,0,0).into(),
                (4,-2,1).into(),
                (2,0,0).into(),
                (8,-4,2).into(),
                (4,-1,0).into(),
                (7,-3,1).into(),
                (16,-9,5).into(),
                (25,-15,9).into(),
                (3,0,-1).into(),
                (3,0,0).into(),
                (12,-6,3).into(),
                (2,1,-2).into(),
                (2,1,-1).into(),
                (2,1,0).into(),
                (5,-1,0).into(),
                (8,-3,1).into(),
                (11,-5,2).into(),
                (4,0,-1).into(),
                (4,0,0).into(),
                (7,-2,0).into(),
                (10,-4,1).into(),
                (19,-10,5).into(),
                (3,1,-2).into(),
                (3,1,-1).into(),
                (3,1,0).into(),
                (6,-1,-1).into(),
                (5,0,-2).into(),
                (4,1,-3).into()
            ])
        );
    }

    #[bench]
    fn many_solutions(b: &mut Bencher) {
        const M: u32 = 2;
        let v: ChernChar::<M> = (45,54,-41).into();
        b.iter(|| left_pseudo_semistabilizers::find_all(&v));
    }
}
