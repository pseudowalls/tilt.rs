#![allow(uncommon_codepoints)]

use bitflags::bitflags;
use pseudo_tilt::chern_character::{ChernChar, Δ};
use pseudo_tilt::tilt_stability::left_pseudo_semistabilizers::find_all;
use pseudo_tilt::tilt_stability::β_;
use std::env;

const M: u32 = 2;

bitflags! {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
    struct RunOptions: u8 {
        const SEMISTABILIZERS = 0b1;
        const WALLS = 0b10;
        const UNSAFE = 0b100;
        const DATA = 0b1000;
    }
}

fn parse_args() -> Result<(ChernChar<M>, RunOptions), &'static str> {
    let mut opts = RunOptions::empty();

    // Collect first three integer-parsible arguments, handling other
    // valid arguments on the way: --<option>
    let int_args: Vec<i64> = env::args()
        .skip(1) // skip executable name
        .filter_map(|arg| match arg.parse::<i64>() {
            Err(_e) => {
                match arg.as_str() {
                    "--semistabilizers" => {
                        opts |= RunOptions::SEMISTABILIZERS;
                    }
                    "--walls" => {
                        opts |= RunOptions::WALLS;
                    }
                    "--unsafe" => {
                        opts |= RunOptions::UNSAFE;
                    }
                    "--data" => {
                        opts |= RunOptions::DATA;
                    }
                    _ => {
                        eprintln!("Non-integer argument: {}", arg);
                    }
                }
                return None;
            }
            Ok(n) => {
                return Some(n);
            }
        })
        .take(3)
        .collect();

    if int_args.len() != 3 {
        eprintln!("Usage: pseudo_tilt [options] r c d");
        eprintln!("    r, c, d integers");
        eprintln!("Options:");
        eprintln!("  --semistabilizers");
        eprintln!("  --walls");
        eprintln!("  --unsafe");
        eprintln!("  --data");
        eprintln!("");
        eprintln!("Incorrect order or format might be ignored without warning");

        return Err("Invalid CLI args");
    }

    if !opts.intersects(RunOptions::SEMISTABILIZERS | RunOptions::WALLS) {
        return Err("One of --semistabilizers or --walls must be specified");
    }

    if opts.contains(RunOptions::WALLS) {
        return Err("--walls not implemented yet");
    }

    if opts.contains(RunOptions::UNSAFE) {
        return Err("--unsafe no longer implemented");
    }

    return Ok((
        ChernChar::<M>::from ((
            int_args[0],
            int_args[1],
            int_args[2],
        )),
        opts,
    ));
}

fn main() -> Result<(), &'static str> {
    // Attempt to parse arguments
    let (v, run_opts) = parse_args()?;

    if !run_opts.contains(RunOptions::DATA) {
        // Preliminary info about given Chern character:
        println!("Chern Character v = {}", v);
        println!("Δ(v) = {}", Δ(&v));

        if let Some(β) = β_(&v) {
            println!("β_(v) = {}", β);
        } else {
            println!("β_(v) is irrational");
        }
    }

    // Act on flags specified:

    if run_opts.contains(RunOptions::SEMISTABILIZERS) {
        if run_opts.contains(RunOptions::DATA) {
            for u in find_all(&v)? {
                let r = u.r;
                let c: i64 = u.c.into();
                let d: i64 = u.d.raw_int_repn();
                println!("{}\t{}\t{}", r, c, d);
            }
        } else {
            println!("");
            println!("pseudo-semistabilizers:");
            for u in find_all(&v)? {
                println!("{u}");
            }
        }
    }
    Ok(())
}
