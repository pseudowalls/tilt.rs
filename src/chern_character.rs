#![allow(dead_code)]

pub mod terms;

use num::rational::Rational64;
use std::fmt::{Display, Formatter};
use serde::{Serialize, Deserialize};
use terms::{Chern0, Chern1, Chern2, cup_product};

/// Represents Chern character of Picard rank 1 surface
#[derive(PartialEq, Eq, Clone, Copy, Debug, Serialize, Deserialize)]
pub struct ChernChar<const M: u32> {
    pub r: terms::Chern0,
    pub c: terms::Chern1,
    pub d: terms::Chern2<M>,
}

impl<const M: u32> ChernChar<M> {
    pub fn new(r: Chern0, c: Chern1, d: Chern2<M>) -> ChernChar<M> {
        ChernChar::<M> { r, c, d }
    }
}
impl<const M: u32> Default for ChernChar<M> {
    fn default() -> ChernChar<M> {
        ( 1, 0, 0 ).into()
    }
}

impl<const M: u32> From<(i64, i64, i64)> for ChernChar<M> {
    fn from(tuple:(i64,i64,i64)) -> ChernChar<M> {
        let (r, c, d) = tuple;
        let r: Chern0 = r;
        let c: Chern1 = c.into();
        let d: Chern2<M> = d.into();
        ChernChar::<M> { r, c, d }
    }
}

impl<const M: u32> Display for ChernChar<M> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {}, {})", self.r, self.c, self.d)
    }
}

// Define addition for Chern Characters on the same variety
impl<const M: u32> std::ops::Add for ChernChar<M> {
    type Output = Self;
    /// Addition of Chern Characters (done component-wise)
    fn add(self, other: Self) -> Self {
        Self {
            r: self.r + other.r,
            c: self.c + other.c,
            d: self.d + other.d,
        }
    }
}
impl<const M: u32> std::ops::Sub for ChernChar<M> {
    type Output = Self;
    /// Addition of Chern Characters (done component-wise)
    fn sub(self, other: Self) -> Self {
        Self {
            r: self.r - other.r,
            c: self.c - other.c,
            d: self.d - other.d,
        }
    }
}

// Multiplication for Chern Characters on the same variety
impl<const M: u32> std::ops::Mul for ChernChar<M> {
    type Output = Self;
    /// Multiplication of Chern Characters
    /// (corresponds to tensor product of underlying objects)
    fn mul(self, other: Self) -> Self {
        Self {
            r: self.r * other.r,
            c: self.r * other.c + other.r * self.c,
            d: self.r * other.d + other.r * self.d
                + cup_product(self.c, other.c),
        }
    }
}

// Left scalar multiplication for ChernChar
impl<const M: u32> std::ops::Mul<ChernChar<M>> for i64 {
    type Output = ChernChar<M>;
    /// multiplication done componentwise
    fn mul(self, other: Self::Output) -> Self::Output {
        let scalar: i64 = self.into();
        Self::Output::new (
            scalar * other.r,
            scalar * other.c,
            scalar * other.d,
        )
    }
}

/// Mumford slope (as a coefficient of ℓ²):
/// ```equation
///      c
/// μ  = -   for v = (r, c⋅ℓ, d⋅ℓ²)
///      r
///```
pub fn μ<const M: u32>(v: &ChernChar<M>) -> Rational64 {
    (v.c.into(), v.r).into()
}

/// Bogomolov discriminant (as a coefficient of ℓ²):
/// ```equation
/// Δ  = c² - 2rd         for v = (r, c⋅ℓ, d⋅ℓ²)
///    = c² - rd'         for v = (r, c⋅ℓ, d'⋅½ℓ²)
///```
#[allow(non_snake_case)]
pub fn Δ<const M: u32>(v: &ChernChar<M>) -> Chern2<M> {
    cup_product::<M>(v.c, v.c) - 2 * v.r * v.d
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display() {
        let r: Chern0 = 1.into();
        let c: Chern1 = 2.into();
        let d: Chern2<2> = (2*2*2).into();
        let v = ChernChar::<2> { r, c, d };
        assert_eq!(format!("{}", v), "(1, 2ℓ, 4ℓ²)");
    }

    #[test]
    fn chern_slope() {
        const M: u32 = 2;
        let v: ChernChar<M> = (1, 1, 1).into();
        assert_eq!(μ(&v), 1.into());
        let v: ChernChar<M> = (2, 4, 5).into();
        assert_eq!(μ(&v), 2.into());
        let v: ChernChar<M> = (3, -1, -3).into();
        assert_eq!(μ(&v), (-1, 3).into());
    }

    #[test]
    fn bogomolov_discriminant() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (1, 1, 1).into();
        assert_eq!(format!("{}", v), "(1, 1ℓ, 1/2ℓ²)");
        assert_eq!(Δ(&v), 0.into());
        let v: ChernChar::<M> = (2, 4, -5).into();
        assert_eq!(Δ(&v), ((16 + 10)*(M as i64)).into());
        let v: ChernChar::<M> = (3, -1, -3).into();
        assert_eq!(Δ(&v), ((1 + 9)*(M as i64)).into());
    }

    #[test]
    fn scalar_mult() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (2, 4, 5).into();
        let scalar: i64 = 3;
        let scalar_mult: ChernChar::<M> = (6, 12, 15).into();
        assert_eq!(scalar_mult, scalar * v);
    }

    #[test]
    fn chern_mult() {
        const M: u32 = 2;
        let v: ChernChar::<M> = (2, 4, 5).into();
        let o = ChernChar::<M>::default(); // structure sheaf

        // check multiplication by structure sheaf does nothing
        assert_eq!(v, o * v);
        assert_eq!(v, v * o);

        // try non-trivial multiplication
        let v_times_v: ChernChar::<M> = (
            2 * 2,
            2 * 4 + 2 * 4,
            5 * 2 + 5 * 2 + 2 * 4 * 4,
        ).into();
        assert_eq!(v_times_v, v * v);
    }
}
