//! Namespace for functions giving twisted Chern character parts.

use crate::chern_character::ChernChar;
use num::Rational64;

/// Returns ch₁^β(v) as a coefficient of ℓ
pub fn chern_1<const M: u32>(v: &ChernChar<M>, β: Rational64) -> Rational64 {
    -β * v.r + i64::from(v.c)
}

/// Returns ch₂^β(v) as a coefficient of ℓ²
pub fn chern_2<const M: u32>(v: &ChernChar<M>, β: Rational64) -> Rational64 {
    v.d.ℓ2_coeff() - β * i64::from(v.c) + β.pow(2) * v.r / 2
}
