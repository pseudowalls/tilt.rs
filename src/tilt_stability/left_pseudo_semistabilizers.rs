//! Module containing all logic related to computing the pseudo-semistabilizers for a
//! fixed Chern character v giving pseudo-walls to the left of the vertical line
//! β=μ(v).
//!
//! The main exposed functions are `find_all` and `find_all_unsafe` both
//! return the pseudo-semistabilizers described above.
//!  - `find_all` returns a vector of all pseudo-semistabilizers.
//!  - `find_all_unsafe` returns a lazy iterator, which takes ownership of the
//!  input ChernChar. This may be faster but that has not been observed.
//!
//!  The former is safer in principal, because by collecting all results on
//!  return, errors can be thrown if any of the computations fail.
//!  Since the latter is lazy, unexpected errors when iterating over the
//!  output, will cause a `panic`, and the error cannot be caught.
//!
//!  This *should* not happen, but the safe version is so fast anyway, there
//!  is little point to using `find_all_unsafe`.
use crate::chern_character::ChernChar;
use super::β_;
use crate::utils::modulo_inverse;
use num::rational::Rational64;

mod fixed_q_beta;

/// Returns i64 iterator which gives all b s.t.
///   ᵇ/ₙ ∈ (0, ch₁^β(v) ) ∩ ¹/ₙ ℤ     where n = β's denominator
///
/// These correspond to all possible values ch₁^β(u) = ᵇ/ₙ can take if u is a
/// pseudo-semistabilizer for v at some stability condition with this fixed β value.
fn considered_b_for_beta<const M: u32>(
    v: &ChernChar<M>,
    β: &Rational64,
) -> impl Iterator<Item = i64> {
    let c: i64 = v.c.into();
    let r: i64 = v.r;
    1..(β.denom() * c - β.numer() * r)
}

pub type ProblemType = u8;
pub const PROBLEM1: ProblemType = 1;
pub const PROBLEM2: ProblemType = 2;

/// data relating to point around which we are searching for walls
/// (currently only (β_, 0))
struct ProblemData<'a, const M: u32, const P: ProblemType> {
    beta: Rational64,
    a_inv: Result<i64, &'static str>,
    v: &'a ChernChar<M>,
}

impl<'a, const M: u32, const P: ProblemType> ProblemData<'a, M, P> {
    fn a(&self) -> &i64 {
        self.beta.numer()
    }
    fn n(&self) -> &i64 {
        self.beta.denom()
    }
    fn v(&self) -> &'a ChernChar<M> {
        self.v
    }
    fn beta(&self) -> &Rational64 {
        &self.beta
    }
}

fn problem_2_data<const M: u32>(v: &ChernChar<M>)
-> Result<ProblemData<M, PROBLEM2>, &'static str>{
    let beta: Rational64 = β_(v)
        // Error out now if β_ is not rational
        // (and hence infinite semistabilizers/walls)
        .ok_or("β_ is not rational: ∞ walls")?;
    let n = beta.denom();
    let a = beta.numer();
    // Should not panic since a and n should be coprime:
    let a_inv = modulo_inverse(a.rem_euclid(*n), *n);

    Ok(ProblemData::<M, PROBLEM2>{ beta, a_inv, v })
}


/// Given a ChernChar v,
/// returns an iterator over all possible pseudo-semistabilizers of v
/// giving pseudo-walls for of the vertical line β=μ(v)
/// if β_ is rational,
///
/// otherwise returns Error.
pub fn find_all<const M: u32>(
    v: &ChernChar<M>,
) -> Result<Vec<ChernChar<M>>, &'static str>
{
    let problem_data = problem_2_data(v)?;

    Ok(considered_b_for_beta(problem_data.v(), problem_data.beta())
        .map(|b|
            // fix ch₁^β = ᵇ/ₙ ℓ of pseudo-semistabilizer
            fixed_q_beta::ProblemData{b, beta_data: &problem_data}
            .find_all()
        ).try_collect::<Vec<_>>()?
        .concat()
    )
}

