//! Namespace for functions giving info about pseudowalls.
//! Typically taking two Chern characters as arguments
//! (corresponding to object being destabilized and the destabilizer)
use crate::chern_character::ChernChar;
use num::rational::Rational64;
use num::CheckedDiv;
use std::cmp::Ordering;

/// calculates the beta coordinate for centre of circle where u and v have same tilt
/// slope
pub fn centre<const M: u32>(u: &ChernChar<M>, v: &ChernChar<M>)
-> Option<Rational64> {
    let divisor: i64 = i64::from(v.c) * u.r - v.r * i64::from(u.c);
    (v.d.ℓ2_coeff() * u.r - u.d.ℓ2_coeff() * v.r)
        .checked_div(&Rational64::from_integer(divisor))
}

/// Calculates the radius squared of centre of circle where u and v have same tilt
/// slope
///   returning None instead of any infinite or negative value
pub fn radius2<const M: u32>(u: &ChernChar<M>, v: &ChernChar<M>)
-> Option<Rational64> {
    let v_c = i64::from(v.c);
    let u_c = i64::from(u.c);
    let beta_coeff: Rational64 = v.d.ℓ2_coeff() * 2 * u.r - u.d.ℓ2_coeff() * 2 * v.r;
    let beta2_coeff: i64 = u_c * v.r - v_c * u.r;
    let const_coeff: Rational64 = u.d.ℓ2_coeff() * 2 * v_c
        - v.d.ℓ2_coeff() * 2 * u_c;

    let radius2 = -const_coeff.checked_div(&beta2_coeff.into())?
        + (beta_coeff.checked_div(&beta2_coeff.into())? / 2).pow(2);

    match radius2.cmp(&Rational64::from_integer(0)) {
        Ordering::Greater => Some(radius2),
        _ => None,
    }
}
