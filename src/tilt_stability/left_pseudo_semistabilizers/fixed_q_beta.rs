//! Module containing all logic which works under the assumption that we are
//! searching for pseudo-walls for a given v, through a fixed β (=a/n) value,
//! and a fixed possible first twisted chern for u (q=b/n) inducing the wall.
//!
//! ```equation
//!        a                           b
//!    β = - (in smallest terms)   q = -
//!        n                           n
//! ```
use crate::chern_character::ChernChar;
use crate::chern_character::terms::{Chern0, Chern1};
use crate::tilt_stability::twisted;
use crate::utils::{least_greater_int, linear_seq};
use num::rational::Rational64;
use num::{CheckedDiv, Integer};
use std::cmp::Ordering;
use super::ProblemData as GeneralProblemData;
use super::{ProblemType, PROBLEM2};

mod fixed_r;

pub struct ProblemData<'a, const M: u32, const P: ProblemType> {
    pub b: i64,
    pub beta_data: &'a GeneralProblemData<'a, M, P>,
}

impl<'a, const M: u32, const P: ProblemType> ProblemData<'a, M, P> {
    pub fn q_val(&self) -> Rational64 {
        (
            self.b,
            *self.beta_data.n()
        ).into()
    }

    /// (inclusive) upper bound for the ranks of pseudo-semistabilizers u
    /// satisfying the assumptions of ContextData
    pub fn r_max(&self) -> Chern0 {
        if P == PROBLEM2 {
            let v = self.beta_data.v;
            let beta = self.beta_data.beta;
            let n = self.beta_data.n();
            let a = self.beta_data.a();
            let b = self.b;
            // TODO express this in terms of bgmlv discriminant
            let twisted_chern_1 = twisted::chern_1(v, beta);
            let q = self.q_val();
            let modulo = ((2*n.pow(2)).lcm(&(M as i64)) / (M as i64)).gcd(&(
                n * a * (2*n.pow(2)).lcm(&(M as i64))
                / (2*n.pow(2))
            ));
            let k_vq = (
                -a * b
                * (M as i64 / (2 * n.pow(2)).gcd(&(M as i64)))
            ).rem_euclid(modulo);
            // need least positive integer with specific mod `modulo` value:
            let k_vq = match k_vq {
                0 => modulo,
                _ => k_vq,
            };

            let bound_1 = q.pow(2)
                * (2 * n.pow(2)).lcm(&(M as i64)) / k_vq / 2;
            let bound_2 =  (twisted_chern_1 - q).pow(2)
                * (2 * n.pow(2)).lcm(&(M as i64)) / k_vq / 2
                + v.r;

            bound_1.min(bound_2).to_integer()
        } else {
            panic!("Not implemented");
        }
    }

    pub fn possible_r(&self)
    -> Result<impl Iterator<Item=(Chern0, Chern1)> + 'a, &'static str> {
        let n = self.beta_data.n();
        let b = self.b;
        let a = self.beta_data.a();
        let a_inv = self.beta_data.a_inv;
        let v = self.beta_data.v();
        // strict lower bound on r to guarantee μ(u) < μ(v):
        let r_lower_bound = Rational64::from_integer(b * v.r)
            .checked_div(&Rational64::from_integer(
                Into::<i64>::into(v.c) * n - a * v.r
            ));

        // non-strict integer lower bound on r to guarantee μ(u) < μ(v):
        let r_int_lower_bound: i64 = match r_lower_bound.map(least_greater_int) {
            Some(bound) => match bound.cmp(&0) {
                Ordering::Greater => bound,
                _ => return Err("Internal logical error: Negative lower bound on r"),
            },
            // Corresponds to case Δ(u)=0, so no circular-left-walls
            None => return Ok(
                (1..=0).step_by(1).zip(linear_seq(0.into(), 0.into()))
            ),
            // this should be empty ^
        };

        // smallest r s.t. a r ≡ -b (mod n) that's at least r_int_lower_bound
        let rstart: i64 = match n.cmp(&1) {
            Ordering::Greater => {
                r_int_lower_bound
                + (-r_int_lower_bound - a_inv? * b).rem_euclid(*n)
            }
            Ordering::Equal => r_int_lower_bound,
            Ordering::Less => return Err("n must be positive"),
        };

        //  c value corresponding to rstart (with the given q=b/n value)
        let cstart: Chern1 = match (b + rstart * a).is_multiple_of(n) {
            true => (b + rstart * a) / n,
            false => {
                return Err("Internal logical error: underlying modular arithmetic")
            }
        }.into();

        let rmax: Chern0 = self.r_max();

        // will panic if n is too big:
        let r_vals = (rstart..=rmax)
            .step_by((*n).try_into().unwrap());
        // will panic if a is too big:
        let c_vals = linear_seq::<Chern1, Chern1>(cstart, (*a).into());

        Ok(r_vals.zip(c_vals))
    }

    pub fn find_all(&self) -> Result<Vec<ChernChar<M>>, &'static str> {
        Ok(self.possible_r()?
            .map(|(r,c)|
                // fix ch₀=r and ch₁=cℓ of pseudo-semistabilizer
                // ChernChars of pseudo-semistabilizers with fixed b, r, c
                fixed_r::ProblemData{r, c, fixed_q_beta_context: self}
                .find_all())
            .try_collect::<Vec<_>>()?
            .concat())
    }
}

