//! Namespace for functions which give bounds for d where
//! u = (r, _ℓ , d·½ℓ²) is a semistabilizer for v at β with ch₁^β(u) = qℓ.
//! The conditions here match those assumed of the parent module.

// FUTURE: Revisit use of function generics here when it's possible to
// specialize over const parameter values
use super::ProblemData as FixedRProblemData;
use crate::tilt_stability::left_pseudo_semistabilizers::{
    PROBLEM1,
    PROBLEM2,
    ProblemType
};

pub mod lower {
    //! Lower bounds for d as per described in parent module
    use num::rational::Rational64;
    use crate::tilt_stability::twisted;
    use super::{PROBLEM1, PROBLEM2, ProblemType, FixedRProblemData};

    pub fn radius_condition<const M: u32, const P: ProblemType>(
        problem: &FixedRProblemData<'_, M, P>
    ) -> Rational64 {
        if P == PROBLEM2 {
            radius_condition_prob2(problem)
        } else if P == PROBLEM1 {
            let r = problem.r;
            let beta = problem.fixed_q_beta_context.beta_data.beta;
            let v = problem.fixed_q_beta_context.beta_data.v;
            radius_condition_prob2(problem) + twisted::chern_2(v, beta) + r / v.r
        } else {
            panic!("Not imlemented");
        }
    }
    pub fn radius_condition_prob2<const M: u32, const P: ProblemType>(
        problem: &FixedRProblemData<'_, M, P>
    ) -> Rational64 {
        let beta = problem.fixed_q_beta_context.beta_data.beta;
        let q = problem.fixed_q_beta_context.q_val();
        let r = problem.r;
        beta.pow(2) * r / 2 + beta * q
    }
}

pub mod upper {
    //! Upper bounds for d as per described in parent module
    use super::lower;
    use crate::tilt_stability::twisted;
    use num::rational::Rational64;
    use super::{PROBLEM1, PROBLEM2, ProblemType, FixedRProblemData};

    /// Returns upper bound on d induced
    /// by the Bogomolov inequality 0 ≤ Δ(u)
    /// where u = (r, _ℓ , d·½ℓ²) with ch₁^β(u) = qℓ
    ///
    /// Returns value as rational if it indeed induces an upper bound,
    /// otherwise None (if say, it induces a lower bound instead)
    pub fn bgmlv1<const M: u32, const P: ProblemType>(
        problem: &FixedRProblemData<'_, M, P>
    ) -> Option<Rational64> {
        // The bound is the same for both probem types
        let q = problem.fixed_q_beta_context.q_val();
        let r = problem.r;
        if r <= 0 {
            return None;
        }

        Some(lower::radius_condition_prob2(problem) + q.pow(2) / (2 * r))
    }

    /// Returns upper bound on d induced
    /// by the Bogomolov inequality 0 ≤ Δ(v-u)
    /// where u = (r, _ℓ , d·½ℓ²) with ch₁^β(u) = qℓ
    ///
    /// Returns value as rational if it indeed induces an upper bound,
    /// otherwise None (if say, it induces a lower bound instead)
    pub fn bgmlv2<const M: u32, const P: ProblemType>(
        problem: &FixedRProblemData<'_, M, P>
    ) -> Option<Rational64> {
        let q = problem.fixed_q_beta_context.q_val();
        let r = problem.r;
        let v = problem.fixed_q_beta_context.beta_data.v;
        let beta = problem.fixed_q_beta_context.beta_data.beta();
        if r <= v.r {
            return None;
        }
        let problem2_bound = lower::radius_condition_prob2(problem)
            + (twisted::chern_1(&v, *beta) - q).pow(2) / (2 * (r - v.r));

        if P == PROBLEM2 {
            Some(problem2_bound)
        } else if P == PROBLEM1{
            Some(
                problem2_bound
                + twisted::chern_2(v, *beta)
            )
        } else {
            panic!("Not implemented");
        }
    }

    /// Returns upper bound on d induced
    /// by the Bogomolov inequality Δ(u) + Δ(v-u) ≤ Δ(v)
    /// where u = (r, _ℓ , d·½ℓ²) with ch₁^β(u) = qℓ
    ///
    /// Returns value as rational if it indeed induces an upper bound,
    /// otherwise None (if say, it induces a lower bound instead)
    pub fn bgmlv3_upperbound_on_d<const M: u32, const P: ProblemType>(
        problem: &FixedRProblemData<'_, M, P>
    ) -> Option<Rational64> {
        if P == PROBLEM2 {
            let q = problem.fixed_q_beta_context.q_val();
            let r = problem.r;
            let v = problem.fixed_q_beta_context.beta_data.v;
            let beta = problem.fixed_q_beta_context.beta_data.beta();
            if 2 * r >= v.r {
                return None;
            }

            Some(
                lower::radius_condition(problem)
                    + (twisted::chern_1(&v, *beta) - q) * q / (v.r - 2 * r),
            )
        } else {
            panic!("Not implemented");
        }
    }
}
