//! Module containing all logic which works under the assumption that we are
//! searching for pseudo-walls for a given v, through a fixed β (=a/n) value, fixed
//! possible first twisted chern for u (q=b/n), and a fixed rank for u, inducing the
//! wall.
//!
//! ```equation
//!        a                           b
//!    β = - (in smallest terms)   q = -
//!        n                           n
//! ```
use crate::chern_character::ChernChar;
use crate::chern_character::terms::{
    Chern0,
    Chern1,
    Chern2,
    least_greater_chern2,
    greatest_lesser_or_eq_chern2,
    inclusive_range
};
use super::ProblemData as GeneralProblemData;
use super::super::ProblemType;

mod bound_on_d;

/// Data relating made for semistabilizers that we are searching for
/// This consists of fixed values of ch₀(u), ch₁(u), on top of
/// assumptions represented by `fixed_q_beta::ContextData`
pub struct ProblemData<'a, const M: u32, const P: ProblemType> {
    pub r: Chern0,
    pub c: Chern1,
    pub fixed_q_beta_context: &'a GeneralProblemData<'a, M, P>,
}

impl<'a, const M: u32, const P: ProblemType> ProblemData<'a, M, P> {
    /// Finds all pseudo_semistabilizers giving 'left' pseudo-walls for v which
    /// satisfy the assumptions in `ContextData`
    pub fn find_all(&self)
    -> Result<Vec<ChernChar<M>>, &'static str> {
        Ok(self.possible_chern2()?
            .map(move |d| ChernChar::<M>::new( self.r, self.c, d ))
            .collect())
    }

    /// Returns possible ch₂ of semistabilizers u of v,
    /// where u satisfies the assumptions of ContextData
    pub fn possible_chern2(&self)
    -> Result<impl Iterator<Item = Chern2<M>>, &'static str> {
        let lowerbound = bound_on_d::lower::radius_condition(self);

        let upperbound = [
            bound_on_d::upper::bgmlv1(self),
            bound_on_d::upper::bgmlv2(self),
          ]
          .iter()
          .filter_map(|x| *x) // Remove None's
          .min()
          .ok_or("logical error, all upper bounds are None")?
          // This error should never happen ^
        ;

        // Return Chern2 values d\ell^2 such that d is between the bounds
        Ok(
            inclusive_range(
                least_greater_chern2(lowerbound),
                greatest_lesser_or_eq_chern2(upperbound)
            )
        )
    }
}
